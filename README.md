# petalmd - test technique - Clément Flodrops

**Dites moi ce que vous pensez de cette musique ;).**

Ce projet a été généré avec @vue/cli.

Vous pouvez donc lancer le webpack-dev-server avec `yarn serve`.

Ce projet ne contient pas :

- de store
- de tests unitaires
- de tests fonctionnels
- de traductions

Tout simplement car c'est un peu de l'overkill pour un tel projet.

Notez que j'aime distinguer 3 types de composants dans mes projets Vue :

- **View** : c'est tout simplement un composant utilisé par le router, classique.
- **Container** : il s'agit d'un composant avec de l'intelligence, capable d'appeler des APIs (ou des actions) etc. Chez Angular on nomme ces composants des Smart Components et chez React, on parle de Container Components.
- **Component** : vous vous en doutez, c'est le composant de rendu, "bête". Chez Angular il s'agit du Dumb Component et chez React du Presentational Component.

Bonne lecture :)

PS : Étant donné la simplicité du code, je n'ai laissé que peu de commentaires.

// In real world app, types would be manage along with the store

export interface Vinyl {
  name: string;
  year: number;
}

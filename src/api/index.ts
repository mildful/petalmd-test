import { Vinyl } from '@/types';
import { CollectionAPIResponse, Release } from './types';

const ENDPOINT_URL = 'https://api.discogs.com/users/ausamerika/collection/folders/0/releases';

export default {

  async getCollectionCount(): Promise<number> {
    // We should have an endpoint just to fetch metadatas about collection (stats)
    return fetch(ENDPOINT_URL)
      .then((res) => res.json())
      .then((res: CollectionAPIResponse) => res.pagination.items)
  },

  async getVinylByIndex(index: number): Promise<Vinyl> {
    // We should have an endpoint to fetch an instance by id
    const page = Math.floor(index / 50); // 50 is the default page size
    const releaseIndex = index - (page * 50); // because arrays stars at 0
    return fetch(`${ENDPOINT_URL}?page=${page}`)
      .then((res) => res.json())
      .then((res: CollectionAPIResponse) => res.releases[releaseIndex])
      .then((r: Release) => ({
        name: r.basic_information.title,
        year: r.basic_information.year,
      }));
  },

}

/*
* Those types are not meant to by used by the application excepted by the API.
* It's just about typing Discogs responses.
*/

export interface Pagination {
  readonly per_page: number;
  readonly items: number;
  readonly page: number;
  readonly urls: {
    last?: string;
    next?: string;
    prev?: string;
    first?: string;
  };
  readonly pages: number;
}

export interface Label {
  readonly name: string;
  readonly entity_type: string;
  readonly catno: string;
  readonly resource_url: string;
  readonly id: number;
  readonly entity_type_name: string;
}

export interface Format {
  readonly descriptions: string[];
  readonly name: string;
  readonly qty: string;
}

export interface Artist {
  readonly join: string;
  readonly name: string;
  readonly anv: string;
  readonly tracks: string;
  readonly role: string;
  readonly resource_url: string;
  readonly id: number;
}

export interface Release {
  readonly instance_id: number;
  readonly date_added: string;
  readonly basic_information: {
    labels: Label[];
    formats: Format[];
    artists: Artist[];
    thumb: string;
    title: string;
    cover_image: string;
    resource_url: string;
    year: number;
    id: number;
  };
  readonly id: number;
  readonly rating: number;
}

export interface CollectionAPIResponse {
  readonly pagination: Pagination;
  readonly releases: Release[];
}
